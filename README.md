# TEST para la empresa PDA con Angular 17

Acá vamos a trabajar en el codigo para el challenge de la empresa PDA trabajado con angular 17.

## 🛠 Herramientas

Angular, Typescript, Bootstrap, Angular Material, Scss, Jest.

## Instalacion

Debe tener disponible node.js para hacer que el siguiente proyecto ande, especialmente npm.
Primero que nada debe clonar el proyecto, y en la terminal acceder a este y ejecutar el siguiente codigo:

```bash
  cd ./angular_test_pda
  npm install
  npm run start // para hacer andar el proyecto
  npm run test // para hacer andar los tests
  npm run test:coverage // para hacer andar los tests con la cobertura cubierta
```

NOTA: La api suministrada "https://api.nasa.gov/mars-photos/api/v1/rovers/{nombre}/photos?sol=1000&api_key=DEMO_KEY" tiene un limite para peticiones, varias veces la consulte y me devolvia un error 429 de api rest con el mensaje "OVER_RATE_LIMIT", por esto, me tome la libertad de generar un mock con datos muy parecidos localmente, y por defecto carga una imagen de que la foto no se pudo cargar correctamente. Esto solo va pasar cuando la api falle, de resto, los datos vienen directamente desde la api, en el apartado network del navegador podra verse en caso que se desee.
