import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NotFoundComponent } from '../../../../../../../src/app/domains/shared/pages/not-found/not-found.component';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser'; // Importar By

describe('NotFoundComponent', () => {
  let component: NotFoundComponent;
  let fixture: ComponentFixture<NotFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NotFoundComponent, RouterTestingModule]
    }).compileComponents();

    fixture = TestBed.createComponent(NotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('should display 404 error message', () => {
    const compiled = fixture.debugElement.nativeElement;
    const h1 = compiled.querySelector('h1');
    expect(h1.textContent).toContain('404');
  });

  test('should display an image', () => {
    const compiled = fixture.debugElement.nativeElement;
    const img = compiled.querySelector('img');
    expect(img).toBeTruthy();
    expect(img.src).toContain('page_not_found.svg');
  });

  test('should display "Pagina no encontrada" message', () => {
    const compiled = fixture.debugElement.nativeElement;
    const p = compiled.querySelector('p.fs-3');
    expect(p.textContent).toContain('Pagina no encontrada');
  });

  test('should have a button to navigate to home', () => {
    const debugElement = fixture.debugElement.query(By.css('a[routerLink="/"]'));
    expect(debugElement).toBeTruthy();
    expect(debugElement.nativeElement.textContent).toBe('Ir al inicio');
  });
});
