import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { By } from '@angular/platform-browser';
import { DetailPhotoComponent } from '@app/domains/mars-photos/components/detail-photo/detail-photo.component';

import { CameraNames, FullName, Photo, RoverNames } from '@app/domains/mars-photos/models/photo.model';
import { CommonModule } from '@angular/common';

describe('DetailPhotoComponent', () => {
  let component: DetailPhotoComponent;
  let fixture: ComponentFixture<DetailPhotoComponent>;

  const mockPhoto: Photo = {
    id: 1,
    sol: 1000,
    camera: {
      id: 1,
      name: CameraNames.FHAZ,
      rover_id: 5,
      full_name: FullName.FrontHazardAvoidanceCamera
    },
    img_src: '../../../../../../src/assets/images/no-image.jpeg',
    earth_date: new Date('2021-01-01'),
    rover: {
      id: 5,
      name: RoverNames.CURIOSITY,
      landing_date: new Date('2012-08-06'),
      launch_date: new Date('2011-11-26'),
      status: 'active',
      max_sol: 1000,
      max_date: new Date('2021-01-01'),
      total_photos: 100,
      cameras: [
        {
          name: CameraNames.FHAZ,
          full_name: FullName.FrontHazardAvoidanceCamera
        }
      ]
    }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CommonModule, DetailPhotoComponent, MatProgressSpinnerModule, MatButtonModule]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('should emit close event when closeDrawer is called', () => {
    jest.spyOn(component.close, 'emit');

    component.closeDrawer();

    expect(component.close.emit).toHaveBeenCalled();
  });

  test('should show spinner when loading is true', () => {
    component.loading = true;
    fixture.detectChanges();

    const spinnerElement = fixture.debugElement.query(By.css('mat-spinner'));
    expect(spinnerElement).toBeTruthy();
  });
});
