import { RoverNames } from '@app/domains/mars-photos/models/photo.model';
import { RoverBadgePipe } from '@app/domains/mars-photos/pipes/rover-badge.pipe';

describe('RoverBadgePipe', () => {
  let pipe: RoverBadgePipe;

  beforeEach(() => {
    pipe = new RoverBadgePipe();
  });

  test('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  test('should return "badge text-bg-success" for RoverNames.CURIOSITY', () => {
    expect(pipe.transform(RoverNames.CURIOSITY)).toBe('badge text-bg-success');
  });

  test('should return "badge text-bg-info" for RoverNames.OPPORTUNITY', () => {
    expect(pipe.transform(RoverNames.OPPORTUNITY)).toBe('badge text-bg-info');
  });

  test('should return "badge text-bg-warning" for RoverNames.SPIRIT', () => {
    expect(pipe.transform(RoverNames.SPIRIT)).toBe('badge text-bg-warning');
  });

  test('should return "badge text-bg-secondary" for unknown rover name', () => {
    expect(pipe.transform('UNKNOWN_ROVER' as RoverNames)).toBe('badge text-bg-secondary');
  });
});
