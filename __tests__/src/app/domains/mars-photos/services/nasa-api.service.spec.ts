import { HttpClient, HttpClientModule, HttpErrorResponse } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { RoverNames, CameraNames, Photo, FullName } from '@app/domains/mars-photos/models/photo.model';
import { NasaApiService } from '@app/domains/mars-photos/services/nasa-api.service';
import { Observable, of, throwError } from 'rxjs';

describe('NasaApiService', () => {
  let httpClient: HttpClient;
  let nasaApiService: NasaApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [NasaApiService]
    });

    httpClient = TestBed.inject(HttpClient);
    nasaApiService = TestBed.inject(NasaApiService);
  });

  test('should be created', () => {
    expect(nasaApiService).toBeTruthy();
  });

  test('getRoverPhotos should return photos from NASA API for a specific rover and camera', (done) => {
    const mockResponse = { photos: [mockPhoto] };
    const rover = RoverNames.CURIOSITY;
    const camera = CameraNames.FHAZ;

    jest.spyOn(httpClient, 'get').mockReturnValueOnce(of(mockResponse));

    nasaApiService.getRoverPhotos(rover, camera).subscribe((photos) => {
      expect(photos.length).toBe(1);
      expect(photos[0]).toEqual(mockPhoto);
      done();
    });
  });

  test('getRoverPhotos should return mock photos on API error', (done) => {
    const rover = RoverNames.CURIOSITY;
    const camera = CameraNames.FHAZ;

    jest.spyOn(httpClient, 'get').mockReturnValueOnce(throwError('API error'));

    nasaApiService.getRoverPhotos(rover, camera).subscribe((photos) => {
      expect(photos.length).toBe(0);
      done();
    });
  });
});

const mockPhoto: Photo = {
  id: 1,
  sol: 1000,
  camera: {
    id: 1,
    name: CameraNames.FHAZ,
    rover_id: 5,
    full_name: FullName.FrontHazardAvoidanceCamera
  },
  img_src: '../../../../../../src/assets/images/no-image.jpeg',
  earth_date: new Date('2021-01-01'),
  rover: {
    id: 5,
    name: RoverNames.CURIOSITY,
    landing_date: new Date('2012-08-06'),
    launch_date: new Date('2011-11-26'),
    status: 'active',
    max_sol: 1000,
    max_date: new Date('2021-01-01'),
    total_photos: 100,
    cameras: [
      {
        name: CameraNames.FHAZ,
        full_name: FullName.FrontHazardAvoidanceCamera
      }
    ]
  }
};
