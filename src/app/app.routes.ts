import { Routes } from '@angular/router';
import { NotFoundComponent } from './domains/shared/pages/not-found/not-found.component';

export const routes: Routes = [
  {
    path: '',
    loadComponent: () => import('./domains/mars-photos/pages/mars-photos-page/mars-photos-page.component').then((m) => m.MarsPhotosPageComponent)
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];
