import { Injectable } from '@angular/core';
import { CameraNames, Filters, RoverNames } from '../models/photo.model';
import { SimpleStoreService } from '@shared/state/simple-store.state';

@Injectable({
  providedIn: 'root'
})
export class MarsPhotosState extends SimpleStoreService<Filters> {
  constructor() {
    super();
    this.setInitialState();
  }

  setInitialState(): void {
    this.state.set({
      rover: RoverNames.CURIOSITY,
      camera: undefined
    });
  }

  setRover(rover: RoverNames): void {
    this.state.set({ rover });
  }

  setCamera(camera: CameraNames): void {
    this.state.set({ camera });
  }
}
