import { Component, inject, OnInit, ViewChild } from '@angular/core';
import { CameraNames, Photo, RoverNames } from '../../models/photo.model';
import { MarsPhotosState } from '../../state/mars-photos.state';
import { MaterialModule } from '../../../shared/material/material.module';
import { PhotoTableComponent } from '../../components/photo-table/photo-table.component';
import { NasaApiService } from '../../services/nasa-api.service';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { MatDrawer } from '@angular/material/sidenav';
import { DetailPhotoComponent } from '../../components/detail-photo/detail-photo.component';

@Component({
  selector: 'app-mars-photos-page',
  standalone: true,
  imports: [MaterialModule, PhotoTableComponent, MatProgressSpinner, DetailPhotoComponent],
  templateUrl: './mars-photos-page.component.html',
  styleUrl: './mars-photos-page.component.scss'
})
export class MarsPhotosPageComponent implements OnInit {
  @ViewChild('drawer') drawer!: MatDrawer;

  private nasaApiService = inject(NasaApiService);
  readonly marsPhotosState = inject(MarsPhotosState);

  readonly state = this.marsPhotosState.state.asReadonly();

  roverNames = Object.values(RoverNames);
  cameraNames = Object.values(CameraNames);

  initialRoverName = RoverNames.CURIOSITY;
  loading = false;
  drawerIsOpen = false;
  tableData: Photo[] = [];
  photoSelected?: Photo;

  ngOnInit(): void {
    this.onSetFilters();
  }

  onRoverChange(rover: RoverNames): void {
    this.marsPhotosState.setRover(rover);
  }

  onCameraChange(camera: CameraNames): void {
    this.marsPhotosState.setCamera(camera);
  }

  onSetFilters(): void {
    this.loading = true;
    const { rover, camera } = this.state();
    this.nasaApiService.getRoverPhotos(rover ?? RoverNames.CURIOSITY, camera).subscribe({
      next: (photos) => {
        if (photos.length > 0) {
          this.tableData = photos;
          this.loading = false;
        }
      },
      error: (e) => {
        console.error(e);
      }
    });
  }

  openDetailPhoto(photo: Photo): void {
    this.photoSelected = photo;
    this.drawer.toggle();
    this.drawerIsOpen = true;
  }

  closeDrawer(): void {
    this.photoSelected = undefined;
    this.drawer.toggle();
    this.drawerIsOpen = false;
  }
}
