import { Pipe, PipeTransform } from '@angular/core';
import { RoverNames } from '../models/photo.model';

@Pipe({
  name: 'roverBadge',
  standalone: true
})
export class RoverBadgePipe implements PipeTransform {
  transform(roverName: RoverNames): string {
    switch (roverName) {
      case RoverNames.CURIOSITY:
        return 'badge text-bg-success';
      case RoverNames.OPPORTUNITY:
        return 'badge text-bg-info';
      case RoverNames.SPIRIT:
        return 'badge text-bg-warning';
      default:
        return 'badge text-bg-secondary';
    }
  }
}
