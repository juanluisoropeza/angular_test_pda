/* eslint-disable no-unused-vars */

export interface Photo {
  id: number;
  sol: number;
  camera: PhotoCamera;
  img_src: string;
  earth_date: Date;
  rover: Rover;
}

export interface PhotoCamera {
  id: number;
  name: CameraNames;
  rover_id: number;
  full_name: FullName;
}

export interface Rover {
  id: number;
  name: RoverNames;
  landing_date: Date;
  launch_date: Date;
  status: string;
  max_sol: number;
  max_date: Date;
  total_photos: number;
  cameras: CameraElement[];
}

export interface CameraElement {
  name: CameraNames;
  full_name: FullName;
}

export interface Filters {
  rover?: RoverNames;
  camera?: CameraNames;
}

export interface PhotoLoadingState {
  loading: boolean;
  showDefaultImage: boolean;
}

export enum FullName {
  ChemistryAndCameraComplex = 'Chemistry and Camera Complex',
  FrontHazardAvoidanceCamera = 'Front Hazard Avoidance Camera',
  MarsDescentImager = 'Mars Descent Imager',
  MarsHandLensImager = 'Mars Hand Lens Imager',
  MastCamera = 'Mast Camera',
  NavigationCamera = 'Navigation Camera',
  RearHazardAvoidanceCamera = 'Rear Hazard Avoidance Camera'
}

export enum RoverNames {
  CURIOSITY = 'Curiosity',
  OPPORTUNITY = 'Opportunity',
  SPIRIT = 'Spirit'
}

export enum CameraNames {
  FHAZ = 'FHAZ',
  RHAZ = 'RHAZ',
  MAST = 'MAST',
  CHEMCAM = 'CHEMCAM',
  MAHLI = 'MAHLI',
  MARDI = 'MARDI',
  NAVCAM = 'NAVCAM'
}
