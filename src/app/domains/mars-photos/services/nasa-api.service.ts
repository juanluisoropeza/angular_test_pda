import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { CameraNames, Photo, RoverNames } from '../models/photo.model';
import { Observable, catchError, map, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NasaApiService {
  private readonly API_KEY = 'DEMO_KEY';
  private readonly BASE_URL = 'https://api.nasa.gov/mars-photos/api/v1/rovers';
  private readonly MOCK_PHOTOS_URL = '../../../../assets/data/data.json';

  private http = inject(HttpClient);

  getRoverPhotos(rover: RoverNames, camera?: CameraNames): Observable<Photo[]> {
    let url = '';
    if (camera) {
      url = `${this.BASE_URL}/${rover}/photos?sol=1000&camera=${camera}&api_key=${this.API_KEY}`;
    } else {
      url = `${this.BASE_URL}/${rover}/photos?sol=1000&api_key=${this.API_KEY}`;
    }
    return this.http.get<{ photos: Photo[] }>(url).pipe(
      map((response) => response.photos),
      catchError(() => {
        console.error('Failed to fetch data from NASA API.');
        return this.getMockPhotos(rover);
      })
    );
  }

  private getMockPhotos(rover: RoverNames): Observable<Photo[]> {
    return this.http.get<Photo[]>(this.MOCK_PHOTOS_URL).pipe(
      map((photos) => photos.filter((photo) => photo.rover.name === rover)),
      catchError(() => {
        console.error('Failed to fetch mock photos.');
        return of([]);
      })
    );
  }
}
