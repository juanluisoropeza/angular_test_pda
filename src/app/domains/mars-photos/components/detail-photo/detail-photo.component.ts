import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Photo } from '../../models/photo.model';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { MatButton } from '@angular/material/button';

@Component({
  selector: 'app-detail-photo',
  standalone: true,
  imports: [MatProgressSpinner, MatButton],
  templateUrl: './detail-photo.component.html',
  styleUrl: './detail-photo.component.scss'
})
export class DetailPhotoComponent implements OnChanges {
  @Input() photoSelected!: Photo;
  @Output() closeSidebar = new EventEmitter<void>();
  loading = true;
  defaultImage = '../../../../../assets/images/no_image_large.png';

  ngOnChanges(changes: SimpleChanges): void {
    if (this.photoSelected && changes['photoSelected']) {
      this.loading = !this.photoSelected;
    }
  }

  closeDrawer(): void {
    this.closeSidebar.emit();
  }
}
