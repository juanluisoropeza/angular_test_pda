import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MaterialModule } from '../../../shared/material/material.module';
import { CommonModule } from '@angular/common';
import { Photo, PhotoLoadingState } from '../../models/photo.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { RoverBadgePipe } from '../../pipes/rover-badge.pipe';

@Component({
  selector: 'app-photo-table',
  standalone: true,
  imports: [MaterialModule, CommonModule, RoverBadgePipe],
  templateUrl: './photo-table.component.html',
  styleUrl: './photo-table.component.scss'
})
export class PhotoTableComponent implements OnInit, AfterViewInit {
  @Input() photos!: Photo[];
  @Output() photoSelected = new EventEmitter<Photo>();
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  elementsByPage = 10;
  resultsLength = 0;
  matSortActive = true;
  displayedColumns: string[] = ['id', 'camera', 'rover', 'photo', 'earth_date', 'actions'];
  dataSource = new MatTableDataSource<Photo>();
  loadingState: Map<number, PhotoLoadingState> = new Map<number, PhotoLoadingState>();

  ngOnInit(): void {
    this.photos.forEach((photo) => this.loadingState.set(photo.id, { loading: false, showDefaultImage: false }));
    this.resultsLength = this.photos.length;
    this.dataSource.data = this.photos;
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.dataSource.sortingDataAccessor = (item: Photo, property: string): string | number => {
      switch (property) {
        case 'camera':
          return item.camera.name;
        case 'id':
          return item.id;
        case 'earth_date':
          return new Date(item.earth_date).getTime();
        default:
          return item.id;
      }
    };
  }

  onImageLoad(photo: Photo): void {
    this.loadingState.set(photo.id, { loading: false, showDefaultImage: false });
  }

  spinnerHidden(photo: Photo): void {
    this.loadingState.set(photo.id, { loading: false, showDefaultImage: true });
  }

  onImageError(photo: Photo): void {
    this.loadingState.set(photo.id, { loading: false, showDefaultImage: true });
  }

  selectedItem(photo: Photo): void {
    this.photoSelected.emit(photo);
  }
}
