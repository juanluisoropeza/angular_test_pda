module.exports = {
  preset: 'jest-preset-angular',
  setupFilesAfterEnv: ['<rootDir>/setup-jest.ts'],
  testRegex: '(/__test__/.*|(\\.|/)(test|spec))\\.ts?$',
  testPathIgnorePatterns: ['<rootDir>/node_modules/'],
  globalSetup: 'jest-preset-angular/global-setup',
  moduleNameMapper: {
    '@app/(.*)$': '<rootDir>/src/app/$1'
  }
};
